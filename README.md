# 需要安装的第三方包

```
npm i element-ui -S
npm install  nprogress -S
npm i axios -S
npm i sass sass-loader -S
npm i vuex@3 -S
npm i vue-router@3 -S
npm i js-cookie@2.2.0 -S
npm i path-to-regexp@2.4.0 -S
```

```
npm i normalize.css


npm install path-to-regexp --save
```

# 一些陌生的 api

```
Document.hidden
Element.getBoundingClientRect()
pathToRegexp
this.$route.matched
```
